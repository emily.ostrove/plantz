import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ListCars from './ListCars';
import CreateManufacturer from './CreateManu';
import CreateModel from './CreateModel';
import CreateAuto from './CreateAuto';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/cars/" element={<ListCars />} />
          <Route path="/cars/new/" element={<CreateAuto />} />
          <Route path="/manufacturer/" element={<CreateManufacturer />} />
          <Route path="/model/" element={<CreateModel />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
