import { React, useState, useEffect } from "react";

export default function CreateAuto(){
    const [auto, setAuto] = useState({
        vin: "",
        model_id: "",
        year: "",
        color: "",
    });

    const [models, setModels] = useState([]);

    useEffect(() => {
        const getModels = async () => {
            const response = await fetch('http://localhost:8100/api/models/');
            const data = await response.json();
            setModels(data.models)
        };
        getModels();
    }, []);
    //console.log("models: ", models)

    const handleSubmit = async(event) => {
        event.preventDefault();
        const data = {...auto}
        const url = 'http://localhost:8100/api/automobiles/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok){
            console.log("it worked!!")
            setAuto({vin: "", model_id: "", year: "", color: ""})
        }
    }

    return (
        <>
            <div className="my-5 container">
                <div className="offset-3 col-6">
                    <form onSubmit={handleSubmit}>
                        <h1>Add a Car</h1>
                        <div className="form-floating mb-3">
                            Car VIN
                            <input onChange={(event) => setAuto({ ...auto, vin: event.target.value })} value={auto.vin}/>
                        </div>
                        <div className="form-floating mb-3">
                            Car Model
                            <select onChange={(event) => setAuto({ ...auto, model_id: event.target.value })} value={auto.model_id}>
                                <option value="models"></option>
                                {models.map(model => {
                                    return(
                                        <option key={model.id} value={model.id}>
                                            {model.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            Year
                            <input onChange={(event) => setAuto({ ...auto, year: event.target.value })} value={auto.year}/>
                        </div>
                        <div className="form-floating mb-3">
                            Color
                            <input onChange={(event) => setAuto({ ...auto, color: event.target.value })} value={auto.color}/>
                        </div>
                        <button type="submit" className="btn btn-primary">
                            Submit
                        </button>
                    </form>
                </div>
            </div>
        </>
    )
}