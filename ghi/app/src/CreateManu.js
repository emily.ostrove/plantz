import { React, useState } from "react";

//how to create a form
export default function CreateManufacturer(){
    const [manufacturer, setManufacturer] = useState("")

    const handleManufacturer = (event) => {
        setManufacturer(event.target.value);
    }

    const handleSubmit = async(event) => {
        event.preventDefault();
        const url = 'http://localhost:8100/api/manufacturers/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(
                {name: manufacturer}),
            headers: {
                "Content-Type": "application/json",
             }       }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            console.log("it worked!")
            const clearState = () => {
                setManufacturer("")
            };
            clearState();
        } else {
            console.log(response.status)
        }
    }
    return(
        <>
            <div className="my-5 container">
                <div className="offset-3 col-6">
                    <h1>Create a manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-manufacturer-form">
                        <div className="form-floating mb-3">
                            <input 
                                onChange={handleManufacturer} 
                                value={manufacturer} 
                                placeholder="Manufacturer name" 
                                required type="text" 
                                name="name" 
                                id="name" 
                                className="form-control"
                            />
                        </div>
                        <button type="submit" className="btn btn-primary">
                            Submit
                        </button>
                    </form>
                </div>
            </div>
        </>
    )
};