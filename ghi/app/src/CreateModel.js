import { React, useState, useEffect } from "react";

export default function CreateModel(){
    const[model, setModel] = useState({
        name: "",
        manufacturer_id: ""
    })

    const[manufacturers, setManufacturers] = useState([])

    useEffect(() => {
        const getManufacturers = async () => {
            const response = await fetch('http://localhost:8100/api/manufacturers/')
            const data = await response.json();
            //console.log("data: ", data)
            setManufacturers(data["manufacturers"]);
        };
        getManufacturers();
    }, []);
    
    const handleSubmit = async(event) => {
        event.preventDefault();
        const data = {...model}
        console.log("checking submit data", data)
        const url = 'http://localhost:8100/api/models/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            console.log("it worked!!")
            setModel({name: "", manufacturer_id: ""})
            //setManufacturers(manufacturers)
        }
    }

    return (
        <>
            <div className="my-5 container">
                <div className="offset-3 col-6">
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            Model name
                            <input onChange={(event) => setModel({ ...model, name: event.target.value })} value={model.name}/>
                        </div>
                        <div className="form-floating mb-3">
                            Select manufacturer
                            <select onChange={(event) => setModel({ ...model, manufacturer_id: event.target.value })}
                            value={model.manufacturer_id}>
                                <option value="manufacturers"></option>
                                {manufacturers.map(manufacturer => {
                                    return(
                                        <option key={manufacturer.id} value={manufacturer.id}>
                                            {manufacturer.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button type="submit" className="btn btn-primary">
                            Submit
                        </button>
                    </form>
                </div>
            </div>
        </>
    )
}