import { React, useState } from "react";

export default function ListCars(){
    const [cars, setCars] = useState([])

    async function getCars() {
        const url = 'http://localhost:8100/api/automobiles/'
        const response = await fetch(url)
        const data = await response.json()
        setCars(data["autos"])
    };
    if (cars.length === 0) {
        getCars()
    }

    //console.log("checking: ", cars, typeof(cars))
    //console.log("one car: ", cars[0], typeof(cars[0]))

    if (cars.length !== 0){
        return (
            <>
                <div className="container">
                    <h1>Let's seem them cars</h1>
                    <div className="row">
                        <table className='table table-striped table-image'>
                        <thead>
                            <tr>
                                <th>Make</th>
                                <th>Model</th>
                                <th>Color</th>
                                <th>VIN</th>
                            </tr>
                        </thead>
                        <tbody>
                            {/* how to map an array of objects */}
                            {cars.map((car) =>
                                <tr key={car.id}>
                                    <td>{car.model.manufacturer.name}</td>
                                    <td>{car.model.name}</td>
                                    <td>{car.color}</td>
                                    <td>{car.vin}</td>
                                </tr>
                            )}
                        </tbody>
                    </table>
                    </div>
                </div>
            </>
        )
    } 
}