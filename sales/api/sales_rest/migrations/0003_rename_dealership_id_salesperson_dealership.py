# Generated by Django 4.0.3 on 2022-12-13 17:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0002_rename_dealership_salesperson_dealership_id'),
    ]

    operations = [
        migrations.RenameField(
            model_name='salesperson',
            old_name='dealership_id',
            new_name='dealership',
        ),
    ]
