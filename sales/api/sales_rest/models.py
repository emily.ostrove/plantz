from django.db import models
from django.urls import reverse

# Create your models here.

class Dealership(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=300)

    def __str__(self):
        return self.pk

    def get_api_url(self):
        return reverse("dealership", kwargs={"pk": self.pk})

class SalesPerson(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveSmallIntegerField(unique=True)
    dealership = models.ForeignKey(
        Dealership,
        related_name="dealership",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name
    
    def get_api_url(self):
        return reverse("api_sales_person", kwargs={"pk": self.employee_number})


class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=300)
    phone_number = models.CharField(max_length=10)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_customer", kwargs={"pk": self.id})


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin

    def get_api_url(self):
        return reverse("api_customer", kwargs={"pk": self.id})


class SaleRecord(models.Model):
    sales_price=models.PositiveIntegerField()
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobiles",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="customer",
        on_delete=models.PROTECT,
    )
    sale_person = models.ForeignKey(
        SalesPerson,
        related_name="sale_person",
        on_delete=models.PROTECT,
    )
    def get_api_url(self):
        return reverse("api_customer", kwargs={"pk": self.id})